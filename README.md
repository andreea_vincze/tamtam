# TamTam page

The application has three routes. The home page shows a carousel and the latest post from the tamtam instagram page. The contact
page displays a contact form featuring validation. The People page is an empty page and does not display anything.


## Build the production bundle
To create the production bundle run ```npm run build```. This will generate the necessary assets in the 'dist' folder.

## Run the application in dev mode
To run the application in dev mode just run ```npm run dev```

## Run the application in production mode
To run the application in production mode run: ```npm run start```

## Compiled code
For simplicity, I pushed the compiled version to this repository as well. It can be found in the ```dist``` folder.

