import React, {Component} from 'react'
import './contact.scss'
import {validateField} from './validate'

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      message: '',
      errors: {},
      showErrorInfo: false,
      isFormSubmitted: false
    }
  }

  validateForm() {
    const {firstName, lastName, email, message}  = this.state;
    const errors = {};
    errors.firstName = validateField('firstName', firstName);
    errors.lastName = validateField('lastName', lastName);
    errors.email = validateField('email', email);
    errors.message = validateField('message', message);
    this.setState({errors});
    return !(errors.firstName || errors.lastName || errors.email || errors.message);
  }

  handleChange = (e) => {
    const {name, value} = e.target;
    const error = validateField(name, value);
    this.setState({[name]: value});
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if(this.validateForm()) {
      this.setState({isFormSubmitted: true});
    }else {
      this.setState({showErrorInfo: true});
    }
  }

  handleBlur = (e) => {
    const {name, value} = e.target;
    const error = validateField(name, value);
    this.setState({errors: {...this.state.errors, [name]: error}});
  }

  getFieldClass (fieldName) {
    return this.state.errors[fieldName] === '' ? 'success' : '';
  }

  render () {
    const {errors, isFormSubmitted} = this.state;
    return (
      <div className="contact">
        <div className="row row--full">
          <h1 className="contact__title">WE WOULD LOVE TO HEAR FROM YOU</h1>
          {isFormSubmitted
            ? <div className="message message--success">Thank you, we have received your message.</div>
            : <form className="form" onSubmit={this.handleSubmit} noValidate>
              {this.state.showErrorInfo && <div className="message">Please complete the form and try again</div>}
              <div className="form__fields">
                <div className="form__row">
                  <div className="form__control">
                    <input type="text" placeholder="First name" name="firstName" className={this.getFieldClass('firstName')} onChange={this.handleChange} onBlur={this.handleBlur} />
                    {errors.firstName && <div className="error__field">{errors.firstName}</div>}
                  </div>
                  <div className="form__control">
                    <input type="text" placeholder="Last name" name="lastName" className={this.getFieldClass('lastName')} onChange={this.handleChange} onBlur={this.handleBlur}/>
                    {errors.lastName && <div className="error__field">{errors.lastName}</div>}
                  </div>
                </div>
                <div className="form__row">
                  <div className="form__control">
                    <input type="email" placeholder="Your e-mail address" name="email" className={this.getFieldClass('email')} onChange={this.handleChange} onBlur={this.handleBlur}/>
                    {errors.email && <div className="error__field">{errors.email}</div>}
                  </div>
                  <div className="form__control">
                    <input type="text" placeholder="Your phone number (optional)" name="phone" onChange={this.handleChange} />
                    {errors.phone && <div className="error__field">{errors.phone}</div>}
                  </div>
                </div>
                <div className="form__row">
                  <div className="form__control form__control--large">
                    <textarea placeholder="Your message..." name="message" className={this.getFieldClass('message')} onChange={this.handleChange} onBlur={this.handleBlur}/>
                    {errors.message && <div className="error__field">{errors.message}</div>}
                  </div>
                </div>
              </div>
              <div className="form__row">
                <button type="submit" className="button button--salmon">Send</button>
              </div>
            </form>
          }
        </div>
      </div>
  );
  }
}

export default Contact;