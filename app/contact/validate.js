const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const validateField = (fieldName, fieldValue) => {
  switch(fieldName) {
    case 'firstName':
      return fieldValue ? '' : 'We need your first name.';
      break;
    case 'lastName':
      return fieldValue ? '' : 'We need your last name.';
      break;
    case 'email':
      if(!fieldValue) {
        return 'We need your email.'
      }
      if(!emailRegex.test(fieldValue)) {
        return 'Please use a valid e-mail address';
      }
      return '';
      break;
    case 'message':
      return fieldValue ? '' : 'Sorry, your message can\'t be empty.';
      break;

    default:
      return '';
  }
}