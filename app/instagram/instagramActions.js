import {handleActions} from 'redux-actions'
import * as types from './instagramTypes'

export const getPhotos = () =>  ({type: types.GET_PHOTOS})
export const photosFound = (photos) => ({type: types.INSTAGRAM_REQUEST_SUCCESS, photos})

export const instagramActionsHandler = handleActions({
  [types.INSTAGRAM_REQUEST_SUCCESS]: (state, action) => ({...state, photos: action.photos})
}, {photos: []})