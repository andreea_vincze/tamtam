import {takeLatest, all, put, call} from 'redux-saga/effects'
import * as instagramTypes from './instagramTypes'
import {photosFound} from './instagramActions'

function getInstagramPhotos () {
  return fetch('https://www.instagram.com/tamtamnl/?__a=1')
    .then(response => response.json())
    .then(result => {
      return result.user.media.nodes.map(node => ({
        image: node.thumbnail_src,
        text: node.caption,
      }))
    })
}

export function * getPhotos() {
  try {
      const photos = yield call(getInstagramPhotos);
      yield put(photosFound(photos))
  } catch(e) {
    console.log(e);
  }
}

export default function * instagramSaga() {
  yield all([
    takeLatest(instagramTypes.GET_PHOTOS, getPhotos)
  ]);
}