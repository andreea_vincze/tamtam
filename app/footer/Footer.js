import React, {Component} from 'react'
import './footer.scss'

class Footer extends Component {
  render () {
    return (
      <div className="footer">
        <a className="footer__icon facebook" href="https://www.facebook.com/tamtamnl" target="_blank"/>
        <a className="footer__icon twitter" href="https://twitter.com/tamtamnl" target="_blank"/>
        <a className="footer__icon instagram" href="https://www.instagram.com/tamtamnl/" target="_blank"/>
      </div>
    );
  }
}

export default Footer;