import React, { Component } from 'react';
import {connect} from 'react-redux'
import smoothScroll from 'smoothscroll';
import SwipeableViews from 'react-swipeable-views';
import {virtualize} from 'react-swipeable-views-utils';
import mod from 'react-swipeable-views-core/lib/mod';
import {getPhotos} from '../instagram/instagramActions'
import './home.scss'
import slide1 from '../images/Walibi.jpg'
import slide2 from '../images/Oxxio.png'
import slide3 from '../images/Florensis.jpg'
import arrowLeft from '../images/arrow-left.svg'
import arrowRight from '../images/arrow-right.svg'

const VirtualizeSwipeableViews = virtualize(SwipeableViews);

const slides = [
  {text: 'WALIBI', image: slide1},
  {text: 'OXXIO', image: slide2},
  {text: 'FLORENSIS', image: slide3}
  ];


class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      carouselIndex: 0
    }
  }

  componentWillMount() {
    this.props.getPhotos();
  }

  changeSlide (nextSlide) {
    this.setState({carouselIndex: nextSlide});
  }

  handleRightClick = () => {
    this.changeSlide(this.state.carouselIndex + 1)
  }

  handleLeftClick = () => {
    this.changeSlide(this.state.carouselIndex - 1);
  }

  handleClickDown = () => {
    smoothScroll(window.innerHeight, 1000);
  }

  slideRenderer = ({key, index}) => {
    const idx = mod(index, slides.length);
    const slide = slides[idx];
    return (
      <div className={`carousel__slide carousel__slide--${index}`} style={{backgroundImage: `url(${slide.image})`}} key={key}>
        <h1 className="carousel__slide__title">{slide.text}</h1>
        <div className="carousel__buttons">
          <div className="carousel__button" onClick={this.handleLeftClick}><img src={arrowLeft} alt="left"/></div>
          <div className="button button--link carousel__button--long">View case</div>
          <div className="carousel__button" onClick={this.handleRightClick}><img src={arrowRight} alt="right"/></div>
        </div>
        <div className="carousel__down" onClick={this.handleClickDown}/>
      </div>
    );
}

  render() {
    const {photos} = this.props;
    const carouselConfig = {
      index: this.state.carouselIndex,
      slideRenderer: this.slideRenderer,
      springConfig: {
        duration: '1s',
        easeFunction: 'cubic-bezier(0.15, 0.3, 0.25, 1)',
        delay: '0s'
      },
      disabled: true
    };

    return (
      <div>
        <div className="carousel">
          <VirtualizeSwipeableViews {...carouselConfig}/>
        </div>
        <div className="row about">
          <h2>WE ARE TAM TAM</h2>
          <p>Tam Tam is a full service digital agency focusing on Dutch Digital Service Design. We combine strategy, design, technology and interaction to make the digital interactions between company and customer valuable and memorable. We work for awesome brands with a team of 120 digitals from our office in Amsterdam. Making great work and having a blast doing it. That’s what we believe in. </p>
        </div>
        <div className="feed">
          <div className="row">
            <h2 className="headline--grey">FOLLOW US ON INSTAGRAM</h2>
            <a className="feed__subtitle link" href="https://www.instagram.com/tamtamnl/" target="_blank">@tamtamnl</a>
            <div className="feed__items">
              {photos && photos.map((item, idx) => (
                <div key={idx} className="feed__item">
                  <img src={item.image} />
                  <div className="feed__item__capture">{item.text}</div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  photos: state.instagram.photos
})

const mapDispatchToProps = dispatch => ({
  getPhotos: () => dispatch(getPhotos())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)