import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Home from 'home/Home';
import App from 'App'
import { store } from './reduxInit';
import {Route, Switch} from 'react-router';
import {HashRouter} from 'react-router-dom'
import Contact from './contact/Contact'

class Routes extends Component {

  render() {
    return (
      <Provider store={store}>
        <HashRouter>
          <App>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/contact' component={Contact}/>
            </Switch>
          </App>
        </HashRouter>
      </Provider>
    );
  }
}

export default Routes;
