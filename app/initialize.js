import 'babel-polyfill';

import ReactDOM from 'react-dom';
import React from 'react';
import Routes from 'Routes';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.getElementById('app');
  ReactDOM.render(<Routes />, el);
});
