import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import {combineReducers} from 'redux';
import {instagramActionsHandler} from './instagram/instagramActions'
import instagramSaga from './instagram/instagramSaga'

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  instagram: instagramActionsHandler
})


const composeEnhancers = compose;
export const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(sagaMiddleware, thunk))
);

sagaMiddleware.run(instagramSaga);
