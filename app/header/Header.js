import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import './header.scss'
import logo from '../images/logo.svg'
import classNames from 'classnames'
class Header extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: [{name: 'Home', link: '/'}, {name: 'People', link: '/people'}, {name:'Contact', link: '/contact'}],
      isMenuOpened: false
    }
  }

  handleMenuIconClick = () => {
    const body = document.getElementsByTagName('body')[0];
    if(this.state.isMenuOpened) {
      body.classList.remove('modal--open');
    } else {
      body.classList.add('modal--open');
    }
    this.setState({isMenuOpened: !this.state.isMenuOpened})
  }

  handleItemClick = () => {
    this.handleMenuIconClick();
  }

  renderDesktopNavigation = () => {
    return (
        <div className="navigation navigation--desktop">
          <NavLink exact to="/"><img src={logo}/></NavLink>
          <ul className="navigation__items">
            {this.state.items.map(item => (
                <li className="navigation__item" key={item.name}>
                  <NavLink exact to={item.link}>{item.name}</NavLink>
                </li>
            ))}
          </ul>
        </div>
    )
  }

  renderMobileNavigation = () => {
    return (
        <div className={classNames('navigation', 'navigation--mobile', {open: this.state.isMenuOpened})}>
          <span className={classNames('navigation__icon')} onClick={this.handleMenuIconClick} />
          <div className="navigation__items navigation__items--centered">
            <img src={logo}/>
          </div>
          <div className={classNames("navigation__menu")}>
            <ul className="navigation__menu__items">
              {this.state.items.map(item => (
                  <li className="navigation__item navigation__menu__item" key={item.name}>
                    <NavLink exact to={item.link} onClick={this.handleItemClick}>{item.name}</NavLink>
                  </li>
              ))}
            </ul>
          </div>
        </div>
    )
  }

  render () {
    return (
        <nav>
          {this.renderDesktopNavigation()}
          {this.renderMobileNavigation()}
        </nav>
    )
  }
}

export default Header